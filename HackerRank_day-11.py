Task:

Calculate the hourglass sum for every hourglass in A, then print the maximum hourglass sum.

CODE:

import math
import os
import random
import re
import sys


arr = []

for i in range(6):
    arr.append(list(map(int,input().rstrip().split())))
    
def get_hourglass_sum(grid,i,j):
    sum = 0
    sum += grid[i-1][j-1]
    sum += grid[i-1][j]
    sum += grid[i-1][j+1]
    sum += grid[i][j]
    sum += grid[i+1][j-1]
    sum += grid[i+1][j]
    sum += grid[i+1][j+1]
   
    return sum
max_hourglass_sum = -63

for i in range(1,5):
    for j in range(1, 5):
        current_hourglass_sum = get_hourglass_sum(arr, i, j)
        if current_hourglass_sum > max_hourglass_sum:
            max_hourglass_sum = current_hourglass_sum

print(max_hourglass_sum)
    
