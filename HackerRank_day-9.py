Task:

Write a factorial function that takes a positive integer n, as a parameter and prints the result of n!.

CODE:

def factorial(n):
    if n<=1:
        return 1
    else:
        return n*factorial(n-1)

n = int(input())
print(factorial(n))

