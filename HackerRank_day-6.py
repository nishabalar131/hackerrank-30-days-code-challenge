Task
Given a string,s , of length n that is indexed from 0 to n-1 , print its even-indexed and odd-indexed characters as 2 space-separated strings on a single line.

Note:0 is considered to be an even index. 

CODE:

n = int(input())
for i in range(0,n):
    s = input()
    even = s[::2]
    odd = s[1::2]
    print(even + " " + odd)



