Task
Given an integer,n , print its first 10 multiples. Each multiple nxi (where 1=<i<10 ) should be printed on a new line in the form:
 n x i = result.

CODE:

if __name__ == '__main__':
    n = int(input())
    for i in range(1,11):
        res = n*i
        print(str(n) + " x " + str(i) + " = " + str(res))
